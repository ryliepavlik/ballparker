.. SPDX-License-Identifier: Apache-2.0
   SPDX-FileCopyrightText: 2020 Collabora, Ltd. and the Ballparker contributors

==========
Ballparker
==========

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README <README>
   introduction
   dsl
   types
   visitors
   phab
   Project Code of Conduct <CODE_OF_CONDUCT>
   License Information <COPYING>
   Changelog <CHANGELOG>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
