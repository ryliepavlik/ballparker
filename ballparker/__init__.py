# Copyright 2019-2020, Collabora, Ltd.
# SPDX-License-Identifier: Apache-2.0
"""Ballparker is a tool for structuring and creating ballpark estimates."""

__version__ = '1.0.2.dev0'
