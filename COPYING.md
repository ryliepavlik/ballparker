# "Ballparker" Licenses

The code itself is Apache License, version 2.0. Some inconsequential config/data
files, as well as template/sample files intended for your modification, are
CC0-1.0. The Code of Conduct, based on the Contributor Covenant version 2.0, is
CC-BY-4.0.

See each file for its license and copyright info, and the `LICENSES` directory for
the full text of relevant licenses. Every file has an SPDX license tag and
copyright or author information in it which should be considered the
authoritative licensing data.

Note that dependencies and third-party documents may have their own licenses.
For instance, the Code of Conduct, based on the Contributor Covenant v2.0, is
licensed CC-BY-4.0.

This project is REUSE 3.0 compliant: all files are marked up with SPDX metadata,
and the REUSE tools can extract and check it.

Contributions are assumed to be on an "incoming == outgoing" basis.
